myShell: myShell.o
	gcc -o myShell myShell.o commandList.o -lreadline 

myShell.o: commandList.o commandList.h 
	gcc -c myShell.c

commandList.o: commandList.c commandList.h
	gcc -c commandList.c

clean:
	rm -f myshell *.o
