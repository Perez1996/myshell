#ifndef COMANDOS_H    // Comienzo del ifdef guard
#define COMANDOS_H


	void printDir();
	
	void init_shell();
	
	int takeInput(char*);
	
	int processString(char*, char**, char**);
	
	void execArgs(char**);
	
	void execArgsPiped(char**, char**);
	
	void init_shell();

#endif // fin del ifdef guard: COMANDOS_HH
