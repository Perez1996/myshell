#!/bin/bash

echo 'Running myShell unit tests...'

assert_equals () {
	if [ "$1" = "$2" ]; then 
		echo -e "$Green $Chech_Mark Success $Color_Off"
	else
		echo -e "$Red Failed $Color_Off"
		exit 1
	fi	
}



echo 'Test: Too many arguments.'
echo './myShell a b c d'
response=$(./myShell a b c d)
assert_equals "$response" "Too many arguments.\n"
